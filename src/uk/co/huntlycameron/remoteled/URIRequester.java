package uk.co.huntlycameron.remoteled;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.os.AsyncTask;
import android.util.Log;

public class URIRequester extends AsyncTask<String, String, String>
{
    private OperateLED caller;
    private static final String TAG = "REMOTE LED ";
    
    public URIRequester(OperateLED a)
    {
        caller = a;
        
    }
    
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);        
    }

    @Override
    protected String doInBackground(String... uri)
    {
        HttpParams httpParameters = new BasicHttpParams();
        // Set the timeout in milliseconds until a connection is established.
        int timeoutConnection = 5000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        // Set the default socket timeout (SO_TIMEOUT)
        // in milliseconds which is the timeout for waiting for data.
        int timeoutSocket = 5000;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

     
        DefaultHttpClient httpclient = new DefaultHttpClient();
        httpclient.setParams(httpParameters);
        
        HttpResponse response;
        String responseString = null;
        try {
            response = httpclient.execute(new HttpGet("http://192.168.1.101:3003/"));
            StatusLine statusLine = response.getStatusLine();
            String content = response.getEntity().toString();
            
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                out.close();
                responseString = out.toString();
                Log.d(TAG, "TOGGLED LIGHT: " + responseString);
            } else{
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
            }
        } catch (ClientProtocolException e) {
            Log.d(TAG, "CLE " + e.toString());
        } catch (IOException e) {
            Log.d(TAG, "IOE " + e.toString());
        }
        return responseString;
    }
    
}
