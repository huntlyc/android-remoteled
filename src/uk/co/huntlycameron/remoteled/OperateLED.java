package uk.co.huntlycameron.remoteled;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class OperateLED extends Activity implements OnClickListener{
    /** Called when the activity is first created. */
    
    private View toggleButton;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        //setup click listeners for the buttons
        toggleButton = findViewById(R.id.btnToggle);
        toggleButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
        case R.id.btnToggle:
            
            //toggleButton.setEnabled(false);
            URIRequester uriReq = new URIRequester(this);
            uriReq.execute("http://192.168.1.101:3003/?eruiu");
            
            break;
        }
    }
    
    public void displayToast(String message){
        Toast.makeText(getApplicationContext(), 
                message,
                Toast.LENGTH_SHORT).show();
        toggleButton.setEnabled(true);
    }
}